import { Component, OnInit, OnChanges } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { AirServiceService } from '../air-service.service';

@Component({
  selector: 'app-air-do-info',
  templateUrl: './air-do-info.component.html',
  styleUrls: ['./air-do-info.component.css']
})
export class AirDoInfoComponent implements OnInit {

  airList:any = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private airOpenAPI: AirServiceService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.airList = [];  
      this._loadData(params['doCode']);
    });
  }

  private _loadData(doCode: string) {
    this.airOpenAPI.getAirInfo(doCode).subscribe(data => {
      let list = this.airOpenAPI.xmlStringToJson(data);

      let resultCode = list['resultCode'],
        resultMessage = list['resultMessage'];

      if (this.airOpenAPI.isError(resultCode)) {
        this.airList = [];
        return;
      }

      this.airList = this.airOpenAPI.parseArrData(list['data']);
    });
  }
}
