import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AirDoInfoComponent } from './air-do-info.component';

describe('AirDoInfoComponent', () => {
  let component: AirDoInfoComponent;
  let fixture: ComponentFixture<AirDoInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AirDoInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AirDoInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
