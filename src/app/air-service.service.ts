import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { NgxXml2jsonService } from 'ngx-xml2json';

@Injectable({
  providedIn: 'root'
})
export class AirServiceService {
  private _serviceKey = 'sBY4Wz%2B1YK%2FvcT5Chc%2BfRUnxnQfXH8mwfzHWaqyd6iJMAazi8tpXnwvY3k3rioJNZMmSJSjra62ycpEZO4gWhQ%3D%3D';
  private _numOfRows = '100';
  private _pageNo = '1';
  private _ver = '1.3'

  constructor(
    private http: HttpClient,
    private ngxXml2jsonService: NgxXml2jsonService
  ) { }

  getAirInfo(sidoName: string) {
    let url = `http://openapi.airkorea.or.kr/openapi/services/rest/ArpltnInforInqireSvc/getCtprvnRltmMesureDnsty?serviceKey=${this._serviceKey}&numOfRows=${this._numOfRows}&pageNo=${this._pageNo}&sidoName=${sidoName}&ver=${this._ver}`;
    return this.http.get(url, { responseType: 'text' });
  }

  xmlStringToJson(data: string) {
    let retObj = {
      resultCode: '',
      resultMessage: '',
      data: []
    };

    let xml = this._getXmlObject(data);
    let json = this.ngxXml2jsonService.xmlToJson(xml)['response'];
    // const comMsgHeader = json['comMsgHeader'];
    const msgHeader = json['header'];
    retObj['resultCode'] = msgHeader['resultCode'];

    if (msgHeader['resultCode'] === '4') {
      retObj['data'] = [];
      retObj['resultMessage'] = msgHeader['resultMsg']
      return retObj;
    }

    retObj['data'] = json['body']['items']['item'];

    return retObj;
  }

  isError(resultCode: string) {
    if (resultCode === '4') {
      return true;
    } else {
      return false;
    }
  }

  parseArrData(data: any) {
    if (data && Array.isArray(data)) {
      return data;
    } else {
      return [data];
    }
  }

  removeEmptyProp(list: any) {
    if (Array.isArray(list)) {
      for (let i = 0; i < list.length; i++) {
        let item = list[i];

        let keys = Object.keys(item);

        for (let key in keys) {
          if (typeof (item[keys[key]]) !== 'string') {
            let subKeys = Object.keys(item[keys[key]]);
            if (subKeys.length === 0) {
              item[keys[key]] = '';
            } else {
              return this.removeEmptyProp(item[keys[key]]);
            }
          }
        }
      }
    }
    return list;
  }

  private _getXmlObject(text: string) {
    let parser = new DOMParser();
    return parser.parseFromString(text, "application/xml");
  }


}
