import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AirDoInfoComponent } from "./air-do-info/air-do-info.component";

const routes: Routes = [
  { path: 'air/:doCode', component: AirDoInfoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
