import { Directive, Input, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appCoValue]'
})
export class CoValueDirective {

  @Input()
  set value(data: string) {
    const val = parseFloat(data);
    let color = 'blue';
    const level1Val = 2;
    const level2Val = 9;
    const level3Val = 15;

    if (val > level1Val && val <= level2Val) {
      color = 'green'
    } else if (val > level2Val && val <= level3Val) {
      color = 'orange'
    } else if (val > level3Val) {
      color = 'red'
    }
    this.renderer.setStyle(this.elementref.nativeElement, 'color', color);
  }

  constructor(
    private elementref: ElementRef, private renderer: Renderer2
  ) { }

}
