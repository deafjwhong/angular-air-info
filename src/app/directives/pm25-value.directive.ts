import { Directive, Input, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appPm25Value]'
})
export class Pm25ValueDirective {

  @Input()
  set value(data: string) {
    const val = parseFloat(data);
    let color = 'blue';
    const level1Val = 15;
    const level2Val = 35;
    const level3Val = 75;

    if (val > level1Val && val <= level2Val) {
      color = 'green'
    } else if (val > level2Val && val <= level3Val) {
      color = 'orange'
    } else if (val > level3Val) {
      color = 'red'
    }
    this.renderer.setStyle(this.elementref.nativeElement, 'color', color);
  }

  constructor(
    private elementref: ElementRef, private renderer: Renderer2
  ) {
  }

}
