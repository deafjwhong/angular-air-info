import { Directive, Renderer2, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appSo2Value]'
})
export class So2ValueDirective {

  @Input()
  set value(data: string) {
    const val = parseFloat(data);
    let color = 'blue';
    const level1Val = 0.03;
    const level2Val = 0.06;
    const level3Val = 0.2;

    if (val > level1Val && val <= level2Val) {
      color = 'green'
    } else if (val > level2Val && val <= level3Val) {
      color = 'orange'
    } else if (val > level3Val) {
      color = 'red'
    }
    this.renderer.setStyle(this.elementref.nativeElement, 'color', color);
  }

  constructor(
    private elementref: ElementRef, private renderer: Renderer2
  ) {
  }
}
