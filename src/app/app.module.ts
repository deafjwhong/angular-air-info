import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AirDoInfoComponent } from './air-do-info/air-do-info.component';
import { So2ValueDirective } from './directives/so2-value.directive';
import { CoValueDirective } from './directives/co-value.directive';
import { O3ValueDirective } from './directives/o3-value.directive';
import { No2ValueDirective } from './directives/no2-value.directive';
import { Pm10ValueDirective } from './directives/pm10-value.directive';
import { Pm25ValueDirective } from './directives/pm25-value.directive';

@NgModule({
  declarations: [
    AppComponent,
    AirDoInfoComponent,
    So2ValueDirective,
    CoValueDirective,
    O3ValueDirective,
    No2ValueDirective,
    Pm10ValueDirective,
    Pm25ValueDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
